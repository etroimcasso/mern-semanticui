import React, { Component } from 'react';
import { Container, Message, Icon, Header } from 'semantic-ui-react';

export default class App extends Component {

    render() {
        return (
            <div className="App">
                <Container textAlign="center">
                    <Message positive floating icon>
                        <Icon name="thumbs up outline" />
                        <Message.Content>
                            <Header as="h1">MERN stack is working!</Header>
                        </Message.Content>
                    </Message>
                </Container>
            </div>
            )
    }
}