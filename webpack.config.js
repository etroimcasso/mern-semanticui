const path = require('path');
const webpack = require('webpack');
require('dotenv').config();

const environment = process.env.NODE_ENV;
const UglifyJsPlugin = require("uglifyjs-webpack-plugin");


module.exports = {
  node: {
    fs: 'empty'
  },
 //mode: 'development',
 entry: './client/index.js',
 output: {
  path: path.join(__dirname, 'client/public'),
  filename: 'bundle.js'
 },
 module: {
  loaders: [
    {
      test: /\.(js|jsx)$/,
      loader: require.resolve('babel-loader'),
      options: {
        compact: environment === 'production',
          // This is a feature of `babel-loader` for webpack (not Babel itself).
          // It enables caching results in ./node_modules/.cache/babel-loader/
          // directory for faster rebuilds.
        //cacheDirectory: isDev,
      },
    },
  {
   test: /\.css$/,
   loader: "style-loader!css-loader"
  },
  {
    test: [/\.eot$/, /\.ttf$/, /\.svg$/, /\.woff$/, /\.woff2$/],
    loader: require.resolve('file-loader'),
    options: {
      name: 'sta/media/[name].[hash:8].[ext]',
    },
  },
  {
    test: [/\.bmp$/, /\.gif$/, /\.jpe?g$/, /\.png$/],
    loader: require.resolve('url-loader'),
    options: {
      limit: 10000,
      name: 'sta/media/[name].[hash:8].[ext]',
    },
  },
  ]
 },
 plugins: [
  new webpack.optimize.ModuleConcatenationPlugin(),
  new webpack.DefinePlugin({          //This will need to go in the production file eventually
    'process.env.NODE_ENV': JSON.stringify(environment)
  }),
  new webpack.optimize.UglifyJsPlugin()
  ]
}