# MERN Skeleton W/ Semantic-UI

This is a simple MERN application skeleton with Semantic-UI configured and installed for ease of creating new MERN applications. Semantic-UI can be easily removed if not desired. 

## Installation

1. Clone the project
2. Change the "name" field in package.json to your project's name
3. Change the "description" field to your project's description
4. `npm install`
5. Set the HTTP_PORT in .env to the port of your choosing (default is 3000)
6. Anytime you change a file in the client directory, `npm run webpack` 
7. `npm run`

## Information

* Nodemon is the default server