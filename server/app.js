require('dotenv').config();

var express = require('express');
var path = require('path');

// Initialize routes
var index = require('./routes/index');

// Initialize Express object
var app = express();

// Set views directory
app.set('views', path.join(__dirname, '../client'));
app.set('view engine', 'pug');


// Register routers with application
app.use('/', index);

//Client Public folder
app.use('/src',express.static(path.join(__dirname, '../client/public')));
app.use('/sta',express.static(path.join(__dirname, '../client/public/sta')));


module.exports = app;
